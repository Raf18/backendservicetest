package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import org.apache.log4j.Logger;

/**
 * This class contains common utilities used in other areas of the project.
 */
public class CommonUtils {

    public static Logger log = Logger.getLogger("root");
    public static FileInputStream fis;
    public static Properties config = new Properties();

    /**
     * Create an INFO log event.
     * @param logMessage The string to be written in the log event
     */
    public static void logInfo(String logMessage) {
        log.info(logMessage);
        TestListener.getTest().info(logMessage);
    }

    /**
     * Create an ERROR log event.
     * @param logMessage The string to be written in the log event
     */
    public static void logError(String logMessage) {
        log.error(logMessage);
        TestListener.getTest().error(logMessage);
    }

    /**
     * Get value for the specified property.
     * @param property The property to retrieve
     * @return Value retrieved for the specified property
     */
    public static String getProperty(String property) {
        try {
            fis = new FileInputStream("src/test/resources/config.properties");
            config.load(fis);
            return config.getProperty(property);
        } catch (IOException e) {
            String errorString = "[PROPERTY] Failed to get property: " + property;
            log.error(errorString);
            TestListener.getTest().error(errorString);
            return "";
        }
    }

}
