package utils;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

/**
 * This class enables test events to trigger actions.
 */
public class TestListener implements ITestListener {

    private static ThreadLocal<ExtentTest> extentTest = new ThreadLocal<>();
    private static ExtentReports extent = ExtentManager.getInstance();
    private static ExtentTest test;
    private static String testName;

    /**
     * Get extentTest.
     * @return extentTest
     */
    public static synchronized ExtentTest getTest() {
        return extentTest.get();
    }

    /**
     * Create an Extent test.
     * @param name Name of the test
     * @param category Category of the test (for tagging)
     * @return test
     */
    public static synchronized ExtentTest createTest(String name, String category) {
        testName = name;
        test = extent.createTest(testName);
        test.assignCategory(category);
        extentTest.set(test);
        return test;
    }

    /**
     * Log an info message against the test.
     * @param message Message to be logged
     */
    public static synchronized void log(String message) {
        test.info(message);
    }

    @Override
    public synchronized void onStart(ITestContext context) {
        CommonUtils.log.info("[TEST] Suite started");
    }

    @Override
    public synchronized void onFinish(ITestContext context) {
        CommonUtils.log.info("[TEARDOWN] Suite ended");
        extent.flush();
    }

    @Override
    public synchronized void onTestStart(ITestResult result) {
    }

    @Override
    public synchronized void onTestSuccess(ITestResult result) {
        extentTest.get().pass("Test passed");
        CommonUtils.log.info("[TEST] " + testName + " PASSED!");
    }

    @Override
    public synchronized void onTestFailure(ITestResult result) {
        extentTest.get().fail(result.getThrowable());
        CommonUtils.log.info("[TEST] " + testName + " FAILED!");
    }

    @Override
    public synchronized void onTestSkipped(ITestResult result) {
        CommonUtils.log.info("[TEST] " + testName + " SKIPPED!");
        extentTest.get().skip(result.getThrowable());
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        System.out.println(("onTestFailedButWithinSuccessPercentage for "
                + testName));
    }

}
