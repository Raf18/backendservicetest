package utils;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

/**
 * This class manages extent reports.
 */
public class ExtentManager {

    private static ExtentReports extent;
    private static String reportFileName = "TestResults.html";
    private static String reportFileLocation = "reports/extent-custom/" + reportFileName;

    /**
     * Create an extent report instance.
     * @return Extent report
     */
    public static ExtentReports getInstance() {
        if (extent == null) {
            String fileName = reportFileLocation;
            ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter(fileName);
            htmlReporter.config().setTheme(Theme.DARK);
            htmlReporter.config().setDocumentTitle(fileName);
            htmlReporter.config().setEncoding("utf-8");
            htmlReporter.config().setReportName(fileName);
            extent = new ExtentReports();
            extent.attachReporter(htmlReporter);
        }
        return extent;
    }

}
