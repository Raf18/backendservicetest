package steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import utils.TestListener;
import utils.CommonUtils;

/**
 * This class initialises.
 */
public class Hooks extends CommonUtils {

    /**
     * Method to run before the Cucumber test suite.
     */
    private void setup() {
        org.apache.log4j.PropertyConfigurator.configure("src/test/resources/log4j.properties");
    }

    /**
     * Create API test.
     * @param scenario The scenario to be executed
     */
    @Before("@api")
    public void setupApi(Scenario scenario) {
        setup();
        TestListener.createTest(scenario.getName(), "api");
    }

    /**
     * Method to run after test suite.
     */
    @After()
    public void tearDown() {
    }

}
