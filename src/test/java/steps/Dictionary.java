package steps;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.CoreMatchers;
import org.testng.Assert;
import utils.CommonUtils;

/**
 * This class stores the API step definitions.
 */
public class Dictionary {

    private Response response;
    private ValidatableResponse json;

    public Dictionary() {
        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setBaseUri(CommonUtils.getProperty("baseUri"))
                .log(LogDetail.URI)
                .build();
    }

    @Given("^I send GET request to \"(.*)\"$")
    public void i_send_get_request_to_x(String endpoint) {
        response = given().when().get(endpoint);
    }

    @Given("^I send POST request to create new post with the following:$")
    public void i_send_post_request_create_new_post_with_the_following(Map<String,String> requestFields) {
        String title = requestFields.get("title");
        String body = requestFields.get("body");
        String postBodyJson = String.format("{\n" +
                "  \"title\": \"%s\",\n" +
                "  \"body\": \"%s\",\n" +
                "  \"userId\": 1\n" +
                "}", title, body);
        response = given()
                    .header("Content-type", "application/json; charset=UTF-8")
                    .body(postBodyJson)
                    .when().post("/posts");
    }

    @Given("^I send POST request to create new comment with the following:$")
    public void i_send_post_request_create_new_comment_with_the_following(Map<String,String> requestFields) {
        String name = requestFields.get("name");
        String email = requestFields.get("email");
        String body = requestFields.get("body");
        String postBodyJson = String.format("{\n" +
                "  \"name\": \"%s\",\n" +
                "  \"email\": \"%s\",\n" +
                "  \"body\": \"%s\",\n" +
                "  \"userId\": 1\n" +
                "}", name, email, body);
        response = given()
                .header("Content-type", "application/json; charset=UTF-8")
                .body(postBodyJson)
                .when().post("/comments");
    }

    @Given("^I send POST request to create new user with the following:$")
    public void i_send_post_request_create_new_user_with_the_following(Map<String,String> requestFields) {
        StringBuilder postBodyJson = new StringBuilder("{\n");
        for (Map.Entry<String, String> field : requestFields.entrySet()) {
            postBodyJson.append(String.format("  \"%s\": \"%s\"\n,", field.getKey(), field.getValue()));
        }
        String postBodyJsonString = postBodyJson.substring(0, postBodyJson.length() - 1);
        postBodyJsonString += ("}");
        response = given()
                .header("Content-type", "application/json; charset=UTF-8")
                .body(postBodyJsonString)
                .when().post("/users");
    }

    @Given("^I send PUT request to update existing post id \"(\\d+)\" with the following:$")
    public void i_send_put_request_update_existing_post_id_x_with_the_following(int postId, Map<String,String> requestFields) {
        String putUri = String.format("/posts/%s", postId);
        String title = requestFields.get("title");
        String body = requestFields.get("body");
        String putBodyJson = String.format("{\n" +
                "  \"title\": \"%s\",\n" +
                "  \"body\": \"%s\",\n" +
                "  \"userId\": 1\n" +
                "}", title, body);
        response = given()
                .header("Content-type", "application/json; charset=UTF-8")
                .body(putBodyJson)
                .when().put(putUri);
    }

    @Given("^I send PUT request to update existing comment id \"(\\d+)\" with the following:$")
    public void i_send_put_request_update_existing_comment_id_x_with_the_following(int commentId, Map<String,String> requestFields) {
        String putUri = String.format("/posts/%s", commentId);
        String name = requestFields.get("name");
        String email = requestFields.get("email");
        String body = requestFields.get("body");
        String putBodyJson = String.format("{\n" +
                "  \"name\": \"%s\",\n" +
                "  \"email\": \"%s\",\n" +
                "  \"body\": \"%s\",\n" +
                "  \"userId\": 1\n" +
                "}", name, email, body);
        response = given()
                .header("Content-type", "application/json; charset=UTF-8")
                .body(putBodyJson)
                .when().put(putUri);
    }

    @Given("^I send PATCH request to \"(.*)\" with the following:$")
    public void i_send_patch_request_to_x_with_the_following(String uri, Map<String,String> requestFields) {
        StringBuilder patchBodyJson = new StringBuilder("{\n");
        for (Map.Entry<String, String> field : requestFields.entrySet()) {
            patchBodyJson.append(String.format("  \"%s\": \"%s\"\n,", field.getKey(), field.getValue()));
        }
        String patchBodyJsonString = patchBodyJson.substring(0, patchBodyJson.length() - 1);
        patchBodyJsonString += ("}");
        response = given()
                .header("Content-type", "application/json; charset=UTF-8")
                .body(patchBodyJsonString.toString())
                .when().patch(uri);
    }

    @Given("^I send DELETE request to \"(.*)\"$")
    public void i_send_delete_request_to_x(String endpoint) {
        response = given().when().delete(endpoint);
    }


    @Given("^I send GET request to \"(.*)\" with the following headers:$")
    public void i_send_get_request_to_x_with_the_following_headers(String endpoint, Map<String,String> responseFields) {
        RequestSpecification request = given();
        for (Map.Entry<String, String> field : responseFields.entrySet()) {
            request.header(field.getKey(), field.getValue());
        }
        response = request.when().get(endpoint);
    }

    @Then("^I expect status code is \"(\\d+)\"$")
    public void i_expect_status_code_is_x(int statusCode) {
        json = response.then().statusCode(statusCode);
    }

    @Then("^I expect status code is NOT \"(\\d+)\"$")
    public void i_expect_status_code_is_not_x(int statusCode) {
        int actual = response.getStatusCode();
        Assert.assertNotEquals(actual, statusCode, String.format(
            "Statues code DOES match unexpected code.\nExpected: NOT %s\nActual: %s\n",
            statusCode, actual));
    }

    @Then("^I expect response time is (less than|greater than) \"(\\d+)\"ms$")
    public void i_expect_response_time_is_x_than_xms(String validation, long expectedResponseTime) {
        long responseTime = response.time();
        System.out.println("Response Time: " + responseTime);
        if (validation.equals("less than")) {
            Assert.assertTrue(responseTime < expectedResponseTime, String.format(
                "Response time is NOT less than expected response time.\nExpected: Less than %s\nActual: %s\n",
                expectedResponseTime, responseTime));
        } else if (validation.equals("greater than")) {
            Assert.assertTrue(responseTime > expectedResponseTime, String.format(
                "Response time is NOT greater than expected response time.\nExpected: Greater than %s\nActual: %s\n",
                expectedResponseTime, responseTime));
        }
    }

    @Then("^I expect response contains the following:$")
    public void i_expect_response_contains_the_following(Map<String,String> responseFields) {
        for (Map.Entry<String, String> field : responseFields.entrySet()) {
            if (StringUtils.isNumeric(field.getValue())) {
                json.body(field.getKey(), equalTo(Integer.parseInt(field.getValue())));
            } else {
                json.body(field.getKey(), equalTo(field.getValue()));
            }
        }
    }

    @Then("^I expect response array contains the following:$")
    public void i_expect_response_array_contains_the_following(Map<String,String> responseFields) {
        for (Map.Entry<String, String> field : responseFields.entrySet()) {
            if (StringUtils.isNumeric(field.getValue())) {
                json.body(field.getKey(), containsInAnyOrder(Integer.parseInt(field.getValue())));
            } else {
                json.body(field.getKey(), CoreMatchers.hasItem(field.getValue()));
            }
        }
    }

    @Then("^I expect response gpath contains the following value:$")
    public void i_expect_response_gpath_contains_the_following(Map<String,String> responseFields) {
        for (Map.Entry<String, String> field : responseFields.entrySet()) {
            String actualValue = response.path(field.getKey());
            Assert.assertEquals(actualValue, field.getValue(), String.format(
                "Gpath value does not match expected value.\nExpected: %s\nActual: %s\n",
                field.getValue(), actualValue));
        }
    }

    @Then("^I expect response gpath contains the following match:$")
    public void i_expect_response_gpath_contains_the_following(List<String> responseFields) {
        for (String field : responseFields) {
            try {
                Assert.assertTrue(response.path(field).toString().length() > 0, String.format(
                    "Response does NOT contain expected match.\nExpected: DOES contain gpath %s\nActual: Does NOT contain gpath %<s\n",
                    field));
            } catch (NullPointerException e) {
                System.out.println(String.format(
                    "Response does NOT contain expected match.\nExpected: DOES contain gpath %s\nActual: Does NOT contain gpath %<s\n",
                    field));
                throw e;
            }
        }
    }

    @Then("^I expect all posts have a title$")
    public void i_expect_all_posts_have_a_title() {
        List<String> postTitle = response.path("title");
        for (int postNum = 0; postNum < postTitle.size(); postNum++) {
            Assert.assertTrue(postTitle.get(postNum).length() > 0, String.format(
                "ERROR: Post with id %s does not have a title", postNum));
        }
    }

    @Then("^I expect all posts have a body")
    public void i_expect_all_posts_have_a_body() {
        List<String> postBody = response.path("body");
        for (int postNum = 0; postNum < postBody.size(); postNum++) {
            Assert.assertTrue(postBody.get(postNum).length() > 0, String.format(
                "ERROR: Post with id %s does not have a body", postNum));
        }
    }

    @Then("^I expect all results have an email address")
    public void i_expect_all_results_have_an_email_address() {
        List<String> responseBody = response.path("email");
        for (int resultNum = 0; resultNum < responseBody.size(); resultNum++) {
            Assert.assertTrue(responseBody.get(resultNum).length() > 0, String.format(
                "ERROR: Result with id %s does not have an email address", resultNum));
        }
    }

    @Then("^I expect user has email address \"(.*)\"")
    public void i_expect_user_has_email_address_x(String emailAddress) {
        int userId = response.path("userId");
        String getBodyJson = String.format("{\n" +
                "  \"userId\": %s\n" +
                "}", userId);
        response = given().body(getBodyJson).when().get("/users");
        List<String> email = response.path("email");
        Assert.assertEquals(email.get(0), emailAddress, String.format(
            "User does not have expected email address.\nExpected: %s\nActual: %s\n",
            emailAddress, email.get(0)));
    }

    @Then("^I expect multiple posts exist for user id \"(\\d+)\"")
    public void i_expect_multiple_posts_exist_for_user_id_x(int userId) {
        List<String> posts = response.path(String.format("findAll { it.userId == %s }", userId));
        Assert.assertTrue(posts.size() > 1, String.format(
            "Multiple posts do not exist for user id %s.\nActual: %s\n",
            userId, posts.size()));
    }

    @Then("^I expect exactly \"(\\d+)\" posts exist for user id \"(\\d+)\"")
    public void i_expect_exactly_x_posts_exist_for_user_id_x(int numPosts, int userId) {
        List<String> posts = response.path(String.format("findAll { it.userId == %s }", userId));
        Assert.assertEquals(posts.size(), numPosts, String.format(
            "Number of posts for user id %s is not as expected.\nExpected: %s\nActual: %s\n",
            userId, numPosts, posts.size()));
    }

    @Then("^I expect response headers contain the following:$")
    public void i_expect_response_headers_contain_the_following(Map<String,String> responseFields) {
        for (Map.Entry<String, String> field : responseFields.entrySet()) {
            if (field.getValue().toLowerCase().equals("notnull")) {
                response.then().header(field.getKey(), notNullValue());
            } else {
                response.then().header(field.getKey(), field.getValue());
            }
        }
    }

    @Then("^I expect response cookies contain the following:$")
    public void i_expect_response_cookies_contain_the_following(Map<String,String> responseFields) {
        for (Map.Entry<String, String> field : responseFields.entrySet()) {
            if (field.getValue().toLowerCase().equals("notnull")) {
                response.then().cookie(field.getKey(), notNullValue());
            } else {
                response.then().cookie(field.getKey(), field.getValue());
            }
        }
    }

}
