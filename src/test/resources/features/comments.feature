@api @comments
Feature: Comments
  As a test automation engineer
  I want to test the Comments API
  So that I have confidence in the quality of the service

  Scenario: Verify GET request for all comments returns 200
    Given I send GET request to "/comments"
    Then I expect status code is "200"

  Scenario: Verify response type is JSON
    Given I send GET request to "/comments"
    Then I expect status code is "200"
    And I expect response headers contain the following:
      | Content-Type | application/json; charset=utf-8 |

  Scenario: Verify response time for all comments request is less than 2 seconds
    Given I send GET request to "/comments"
    Then I expect response time is less than "2000"ms

  Scenario: Verify response time for single comment request is less than 2 seconds
    Given I send GET request to "/comments/1"
    Then I expect response time is less than "2000"ms

  Scenario: Verify response cookie
    Given I send GET request to "/comments"
    Then I expect status code is "200"
    And I expect response cookies contain the following:
      | __cfduid | notnull |

  Scenario: Verify all comments have an email address
    Given I send GET request to "/comments"
    Then I expect all results have an email address

  Scenario: Verify GET request for all comments associated with a post
    Given I send GET request to "/comments?postId=1"
    Then I expect status code is "200"

  Scenario: Verify email address associated with comment
    Given I send GET request to "/comments/400"
    Then I expect status code is "200"
    And I expect response contains the following:
      | email | Brennon@carmela.tv |

  Scenario: Verify name associated with comment
    Given I send GET request to "/comments/450"
    Then I expect status code is "200"
    And I expect response contains the following:
      | name | dolorem corporis facilis et |

  Scenario: Verify email address of comment with specific name
    Given I send GET request to "/comments"
    Then I expect status code is "200"
    And I expect response gpath contains the following value:
      | find { it.name == 'quisquam laborum reiciendis aut' }.email | Alessandra.Nitzsche@stephania.us |

  Scenario: Verify a new comment can be created with name, email and body
    Given I send POST request to create new comment with the following:
      | name  | Name of the comment |
      | email | test@test.com       |
      | body  | Body of the comment |
    Then I expect status code is "201"
    And I expect response contains the following:
      | name  | Name of the comment |
      | email | test@test.com       |
      | body  | Body of the comment |

  Scenario: Verify a new comment can be created with empty name
    Given I send POST request to create new comment with the following:
      | name  |                     |
      | email | test@test.com       |
      | body  | Body of new comment |
    Then I expect status code is "201"
    And I expect response contains the following:
      | name  |                     |
      | email | test@test.com       |
      | body  | Body of new comment |

  Scenario: Verify a new comment can be created with empty email
    Given I send POST request to create new comment with the following:
      | name  | Name of new comment |
      | email |                     |
      | body  | Body of new comment |
    Then I expect status code is "201"
    And I expect response contains the following:
      | name  | Name of new comment |
      | email |                     |
      | body  | Body of new comment |

  Scenario: Verify a new comment can be created with empty body
    Given I send POST request to create new comment with the following:
      | name  | Name of new comment |
      | email | test@test.com       |
      | body  |                     |
    Then I expect status code is "201"
    And I expect response contains the following:
      | name  | Name of new comment |
      | email | test@test.com       |
      | body  |                     |

  Scenario: Verify an existing comment can be updated with new name, email, body
    Given I send PUT request to update existing comment id "1" with the following:
      | name  | Existing name updated |
      | email | testUpdated@test.com  |
      | body  | Existing body updated |
    Then I expect status code is "200"
    And I expect response contains the following:
      | name  | Existing name updated |
      | email | testUpdated@test.com  |
      | body  | Existing body updated |

  Scenario: Verify an existing comment can be patched with new name
    Given I send PATCH request to "/comments/1" with the following:
      | name  | Existing name patched |
    Then I expect status code is "200"
    And I expect response contains the following:
      | name  | Existing name patched |
      | email | Eliseo@gardner.biz    |

  Scenario: Verify an existing comment can be patched with new email
    Given I send PATCH request to "/comments/1" with the following:
      | email  | testPatch@test.com |
    Then I expect status code is "200"
    And I expect response contains the following:
      | name  | id labore ex et quam laborum |
      | email | testPatch@test.com           |

  Scenario: Verify an existing comment can be deleted
    Given I send DELETE request to "/comments/1"
    Then I expect status code is "200"