@api @e2e
Feature: E2E
  As a test automation engineer
  I want to test the end to end relationship between APIs
  So that I have confidence in the quality of the service

  Scenario: Verify email address of user associated with a post
    Given I send GET request to "/posts/1"
    Then I expect status code is "200"
    And I expect user has email address "Sincere@april.biz"

  Scenario: Verify user can be associated with multiple posts
    Given I send GET request to "/posts"
    Then I expect status code is "200"
    And I expect multiple posts exist for user id "1"

  Scenario: Verify number of posts by user
    Given I send GET request to "/posts"
    Then I expect status code is "200"
    And I expect exactly "10" posts exist for user id "2"