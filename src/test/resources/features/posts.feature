@api @posts
Feature: Posts
  As a test automation engineer
  I want to test the Posts API
  So that I have confidence in the quality of the service

  Scenario: Verify GET request for all posts returns 200
    Given I send GET request to "/posts"
    Then I expect status code is "200"

  Scenario: Verify response type is JSON
    Given I send GET request to "/posts"
    Then I expect status code is "200"
    And I expect response headers contain the following:
      | Content-Type | application/json; charset=utf-8 |

  Scenario: Verify response time for all posts request is less than 2 seconds
    Given I send GET request to "/posts"
    Then I expect response time is less than "2000"ms

  Scenario: Verify response time for single post request is less than 2 seconds
    Given I send GET request to "/posts/1"
    Then I expect response time is less than "2000"ms

  Scenario: Verify response cookie
    Given I send GET request to "/posts"
    Then I expect status code is "200"
    And I expect response cookies contain the following:
      | __cfduid | notnull |

  Scenario: Verify all posts have title and body
    Given I send GET request to "/posts"
    Then I expect all posts have a title
    And I expect all posts have a body

  Scenario: Verify GET request for all posts by userId 5 is NOT forbidden
    Given I send GET request to "/posts?userId=5"
    Then I expect status code is NOT "403"

  Scenario: Verify title of post 98
    Given I send GET request to "/posts/98"
    Then I expect status code is "200"
    And I expect response contains the following:
      | title | laboriosam dolor voluptates |

  Scenario: Verify post exists with specific title
    Given I send GET request to "/posts"
    Then I expect status code is "200"
    And I expect response gpath contains the following match:
      | find { it.title == 'qui est esse' } |

  Scenario: Verify a new post can be created with title and body
    Given I send POST request to create new post with the following:
      | title | Title of the post |
      | body  | Body of the post  |
    Then I expect status code is "201"
    And I expect response contains the following:
      | id    | 101               |
      | title | Title of the post |
      | body  | Body of the post  |

  Scenario: Verify a new post can be created with empty title
    Given I send POST request to create new post with the following:
      | title |                  |
      | body  | Body of new post |
    Then I expect status code is "201"
    And I expect response contains the following:
      | title  |                  |
      | body   | Body of new post |

  Scenario: Verify a new post can be created with empty body
    Given I send POST request to create new post with the following:
      | title  | Title of new post |
      | body   |                   |
    Then I expect status code is "201"
    And I expect response contains the following:
      | title  | Title of new post |
      | body   |                   |

  Scenario: Verify an existing post can be updated with new title and body
    Given I send PUT request to update existing post id "1" with the following:
      | title | Existing title updated |
      | body  | Existing body updated  |
    Then I expect status code is "200"
    And I expect response contains the following:
      | id     | 1                      |
      | title  | Existing title updated |
      | body   | Existing body updated  |

  Scenario: Verify an existing post can be patched with new title
    Given I send PATCH request to "/posts/1" with the following:
      | title | Existing title patched |
    Then I expect status code is "200"
    And I expect response contains the following:
      | title  | Existing title patched |

  Scenario: Verify an existing post can be patched with new body
    Given I send PATCH request to "/posts/2" with the following:
      | body | Existing body patched |
    Then I expect status code is "200"
    And I expect response contains the following:
      | body  | Existing body patched |
      | title | qui est esse          |

  Scenario: Verify an existing post can be deleted
    Given I send DELETE request to "/posts/1"
    Then I expect status code is "200"