@api @users
Feature: Users
  As a test automation engineer
  I want to test the Users API
  So that I have confidence in the quality of the service

  Scenario: Verify GET request for all users returns 200
    Given I send GET request to "/users"
    Then I expect status code is "200"

  Scenario: Verify response type is JSON
    Given I send GET request to "/users"
    Then I expect status code is "200"
    And I expect response headers contain the following:
      | Content-Type | application/json; charset=utf-8 |

  Scenario: Verify response time for all comments request is less than 2 seconds
    Given I send GET request to "/users"
    Then I expect response time is less than "2000"ms

  Scenario: Verify response time for single user request is less than 2 seconds
    Given I send GET request to "/users/1"
    Then I expect response time is less than "2000"ms

  Scenario: Verify response cookie
    Given I send GET request to "/users"
    Then I expect status code is "200"
    And I expect response cookies contain the following:
      | __cfduid | notnull |

  Scenario: Verify all users have an email address
    Given I send GET request to "/users"
    Then I expect all results have an email address

  Scenario: Verify username and zip code associated with user
    Given I send GET request to "/users/1"
    Then I expect status code is "200"
    And I expect response contains the following:
      | username        | Bret       |
      | address.zipcode | 92998-3874 |

  Scenario: Verify city of user with specific username
    Given I send GET request to "/users"
    Then I expect status code is "200"
    And I expect response gpath contains the following value:
      | find { it.username == 'Maxime_Nienow' }.address.city | Aliyaview |

  Scenario: Verify a new user can be created with name, username and email address
    Given I send POST request to create new user with the following:
      | name     | Bob Smith         |
      | username | bsmith123         |
      | email    | bobsmith@test.com |
    Then I expect status code is "201"
    And I expect response contains the following:
      | name     | Bob Smith         |
      | username | bsmith123         |
      | email    | bobsmith@test.com |

  Scenario: Verify a new user can be created with empty name
    Given I send POST request to create new user with the following:
      | name     |                   |
      | username | bsmith123         |
      | email    | bobsmith@test.com |
    Then I expect status code is "201"
    And I expect response contains the following:
      | name     |                   |
      | username | bsmith123         |
      | email    | bobsmith@test.com |

  Scenario: Verify a new user can be created with empty username
    Given I send POST request to create new user with the following:
      | name     | Bob Smith         |
      | username |                   |
      | email    | bobsmith@test.com |
    Then I expect status code is "201"
    And I expect response contains the following:
      | name     | Bob Smith         |
      | username |                   |
      | email    | bobsmith@test.com |

  Scenario: Verify a new user can be created with empty email address
    Given I send POST request to create new user with the following:
      | name     | Bob Smith         |
      | username | bsmith123         |
      | email    |                   |
    Then I expect status code is "201"
    And I expect response contains the following:
      | name     | Bob Smith         |
      | username | bsmith123         |
      | email    |                   |

  Scenario: Verify an existing user can be patched with new name, username and email address
    Given I send PATCH request to "/users/1" with the following:
      | name     | Mike Harris       |
      | username | mharris2          |
      | email    | mharris3@test.com |
    Then I expect status code is "200"
    And I expect response contains the following:
      | name         | Mike Harris       |
      | username     | mharris2          |
      | email        | mharris3@test.com |
      | address.city | Gwenborough       |

  Scenario: Verify an existing user can be deleted
    Given I send DELETE request to "/users/1"
    Then I expect status code is "200"