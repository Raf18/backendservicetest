# Backend Service Test

This repo contains a test automation framework for API testing.

The tests are implemented using Java, Cucumber, Gherkin, Gradle and TestNG.  

## Pre Requisites:

Clone this repo using `git clone https://gitlab.com/Raf18/[repo-name.git]`  

The following items are suggested pre-requisites for running the automated test suite, based on the setup used during development.

### JDK 8
https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html

Once installed, set JAVA_HOME (https://docs.oracle.com/cd/E19182-01/820-7851/inst_cli_jdk_javahome_t/)

### Gradle
https://gradle.org/install/

### IntelliJ IDEA Community Edition
https://www.jetbrains.com/idea/download/#section=mac


## Test Execution
Open IntelliJ and ensure JDK is configured correctly by navigating to the following from the welcome splash screen: `Configure > Project Defaults > Project Structure`

Once all pre-requisites are installed, open the project in IntelliJ.
* Tick 'Use auto-import' if option is displayed, else click 'Import changes' when 'Gradle projects need to be imported' notification is displayed.
* Specify Gradle home directory (To enable field, select 'Use local gradle distribution') 
* If installed, uninstall Substeps plugin from `IntelliJ IDEA > Preferences > Plugins`
* Install 'Cucumber for Java' and 'Gherkin' plugins 

There are two methods for executing tests:

A) Navigate to the project directory using the command line and run the following command  
`gradle test --rerun-tasks`

The gradle task runs the TestNG suite "cucumberApi.xml". This runs the Cucumber Runner which is configured to look for Cucumber tests with the tags `@api`.  

The baseUri is currently set to `https://jsonplaceholder.typicode.com` in `src/test/resources/config.properties`  

Parallel test execution can be achieved by creating an additional runner and adding it to the "cucumberApi.xml" TestNG suite.

B) In IntelliJ, right-click on `src/test/resources/features/[fileName].feature` and select 'Run Feature: fileName.feature'

## Logging and Reporting

Logs are output to the following file:  
`src/test/resources/logs/application.log`

A Cucumber specific report is output to the following file:  
`reports/TestResults.html`



